package net.openesb.runtime.gfv4;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.jbi.JBIException;
import javax.transaction.TransactionManager;

import org.glassfish.api.StartupRunLevel;
import org.glassfish.api.event.EventListener;
import org.glassfish.api.event.EventTypes;
import org.glassfish.api.event.Events;
import org.glassfish.hk2.api.PostConstruct;
import org.glassfish.hk2.api.PreDestroy;
import org.glassfish.hk2.runlevel.RunLevel;
import org.glassfish.internal.api.ServerContext;
import org.jvnet.hk2.annotations.Service;

import com.sun.enterprise.config.serverbeans.Config;
import com.sun.enterprise.config.serverbeans.Domain;
import com.sun.enterprise.transaction.api.RecoveryResourceRegistry;
import com.sun.enterprise.transaction.spi.RecoveryResourceListener;
import com.sun.jbi.StringTranslator;

/**
 * This is the top-level class that provides the lifecycle for the JBI
 * framework. It implements the GlassFish <CODE>EventListener</CODE> interface
 * to allow the JBI runtime framework to start automatically when the GlassFish
 * starts, and stop automatically when the GlassFish stops. The one method
 * required by the <CODE>EventListener</CODE> interface is <CODE>event()</CODE>,
 * which handles GlassFish life cycle events for startup, ready and shutdown.
 * Each event is handled by a corresponding method in this class, as follows:
 *
 * <UL>
 * <LI>SERVER_STARTUP ---> <CODE>startup()</CODE></LI>
 * <LI>SERVER_READY ---> <CODE>ready()</CODE></LI>
 * <LI>PREPARE_SHUTDOWN ---> <CODE>shutdown()</CODE></LI>
 * </UL>
 *
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
@Service
@RunLevel(StartupRunLevel.VAL)
public class GlassFishJBIFramework extends com.sun.jbi.framework.JBIFramework implements PreDestroy, PostConstruct, EventListener, Runnable,
        RecoveryResourceListener {

    private static final String STRING_TRANSLATOR_NAME = "net.openesb.runtime.gfv4";

    private static final String VERSION = "4.0.0-b03";

    @Inject
    ServerContext context;

    @Inject
    Events events;

    @Inject
    Domain domain;

    @Inject
    Config config;

    @Inject
    private RecoveryResourceRegistry recoveryListenersRegistry;

    /**
     * Glassfish platform details.
     */
    private GlassFishPlatformContext mPlatform;

    /**
     * Sync thread. Will be running if synchronization is required until
     * completed.
     */
    private Thread mSyncThread;
    private RegistryHelper mRegistryHelper;
    private SyncProcessor mSyncProcessor;
    private Logger mLogger;
    private StringTranslator mTranslator;

    /**
     * Last event received.
     */
    private String mLastEvent;

    private long mStartStamp;
    private long mStartTime;

    /**
     * Tracks the current phase of the Framework. This is used to block activity
     * that depends on being in a particular phase.
     */
    private int mPhase;
    private static final int PHASE_NONE = 0;
    private static final int PHASE_INIT = 1;
    private static final int PHASE_STARTUP = 2;
    private static final int PHASE_PREPARE = 3;
    private static final int PHASE_READY = 4;
    private static final int PHASE_SHUTDOWN = 5;
    private static final int PHASE_TERMINATE = 6;

    @Override
    public void postConstruct() {
        mStartStamp = System.currentTimeMillis();
        mPhase = PHASE_NONE;
        mLogger = Logger.getLogger("com.sun.jbi.framework");
        mLogger.info("OpenESB for GlassFish (4.1) starting...");
        mPlatform = new GlassFishPlatformContext(context, domain, config);
        events.register(this);
        recoveryListenersRegistry.addListener(this);
        Properties props = new Properties();
        props.setProperty("com.sun.jbi.home", System.getProperty("com.sun.aas.installRoot") + "/jbi");
        try {
            super.init(mPlatform, props);
        } catch (JBIException e) {
            e.printStackTrace();
        }
        mTranslator = getEnvironment().getStringTranslator(STRING_TRANSLATOR_NAME);
        mRegistryHelper = new RegistryHelper(getEnvironment());
        mSyncProcessor = new SyncProcessor(getEnvironment(), mRegistryHelper, config.getNetworkConfig());
        endPhase(PHASE_INIT);
        mLogger.log(Level.FINE, "JBI:Start Time 0: {0}", (System.currentTimeMillis() - mStartStamp));
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void event(Event event) {
        try {
            if (event.is(EventTypes.SERVER_STARTUP)) {
                if (null != getEnvironment()) {
                    super.startup(mPlatform.getNamingContext(), "");
                    endPhase(PHASE_STARTUP);
                    mLogger.log(Level.FINE, "JBI:Start Time 1: {0}", (System.currentTimeMillis() - mStartStamp));
                }
            } else if (event.is(EventTypes.SERVER_READY)) {
                if (null != getEnvironment()) {
                    long startTime = System.currentTimeMillis();
                    // Perform intial processing (e.g. config) for system components
                    bootstrapSystemComponents();

					//
                    //
                    boolean isStartupActive = mRegistryHelper.isActiveStartup();
                    mStartTime += System.currentTimeMillis() - startTime;
                    if (isStartupActive) {
						//
                        // If we are going from STARTUP to READY, synthesize a
                        // PREPARE so that the underlying code doesn't have to
                        // track the AS-specific behavior,
                        //
                        if (EventTypes.SERVER_STARTUP.type().equals(mLastEvent)) {
                            mLogger.fine("JBI:ActiveStartup");
                            super.prepare();
                            endPhase(PHASE_PREPARE);
                        }
                        ready();
                        endPhase(PHASE_READY);
                    } else {
                        enterLazyMode();
                    }
                }
                mLogger.log(Level.FINE, "JBI:Start Time 2: {0}", (System.currentTimeMillis() - mStartStamp));
            } else if (event.is(EventTypes.PREPARE_SHUTDOWN)) {
                stopSync();
                if (null != getEnvironment()) {
                    super.shutdown();
                }
                endPhase(PHASE_SHUTDOWN);
            }
            mLastEvent = event.type().type();
        } catch (JBIException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void preDestroy() {
        stopSync();
        if (null != getEnvironment()) {
            try {
                super.terminate();
            } catch (JBIException e) {
                e.printStackTrace();
            }
        }
        endPhase(PHASE_TERMINATE);
    }

    /**
     * The GF Lifecycle interface will call this method after START and before
     * READY to allow local XAResources to be declared for recovery.
     */
    @Override
    public javax.transaction.xa.XAResource[] getXAResources() {
		//
        // Just in case GF calls us while STARTUP is still active, we will wait until
        // we know the parts of startup that interest us have completed.
        //
        mLogger.log(Level.FINE, "JBI:getXAResources: {0}", (System.currentTimeMillis() - mStartStamp));

        waitForPhase(PHASE_STARTUP);

		//
        // See if need to start the JBI framework for XA resource recovery resources
        //
        if (mSyncProcessor != null) {
            try {
                long startTime = System.currentTimeMillis();
                boolean isStartupActive = mRegistryHelper.isActiveStartup();
                mStartTime += System.currentTimeMillis() - startTime;
                setStartupTime();
                if (isStartupActive) {
                    mLogger.fine("JBI:XAActiveStartup");
                    super.prepare();
                    endPhase(PHASE_PREPARE);
                    mLastEvent = "";
                }
            } catch (Exception e) {
                mLogger.warning(mTranslator.getString(LocalStringKeys.GET_XA_RESOURCES, e));
            }
        }

		//
        // At this point we should have collected all local XAResources declared by JBI Components.
        //
        return (getEnvironment().getNormalizedMessageService().getXAResources());
    }

    /**
     * Performs one-time initialization of JBI system components during startup.
     * Exceptions during component bootstrap are (a) unlikely and (b) not
     * considered fatal, so any exceptions are just dumped to the server log to
     * assist in debugging.
     */
    private void bootstrapSystemComponents() {
        SystemComponentBootstrap bootstrap;

        try {
            bootstrap = new SystemComponentBootstrap(getEnvironment(), config.getNetworkConfig());
            bootstrap.configureHttpSoapDefaultPorts();
        } catch (Exception ex) {
            mLogger.log(Level.INFO, mTranslator.getString(LocalStringKeys.SYSTEM_COMPONENT_INIT_EXCEPTION), ex);
        }
    }

    /**
     * Get the implementation of the TransactionManager.
     *
     * @return the TransactionManager implementation.
     * @throws Exception if the TransactionManager instance cannot be obtained.
     */
    public TransactionManager getTransactionManager() throws Exception {
        TransactionManager transactionManager = (javax.transaction.TransactionManager) getEnvironment().getNamingContext().lookup(
                "java:appserver/TransactionManager");
        return transactionManager;
    }

	// -------------------------ResourceRecoveryListener---------------------------------
    /**
     * Callback stating the XAResource recovery has started.
     */
    @Override
    public void recoveryStarted() {
        mLogger.log(Level.FINE, "JBI:recoveryStarted: {0}", (System.currentTimeMillis() - mStartStamp));

    }

    /**
     * Callback stating that XAResource recovery has completed. We purge an XA
     * resource registrations held by the Message Service so as to not hold on
     * to any memory if the component is shutdown.
     */
    @Override
    public void recoveryCompleted() {
        getEnvironment().getNormalizedMessageService().purgeXAResources();
        mLogger.log(Level.FINE, "JBI:recoveryCompleted: {0}", (System.currentTimeMillis() - mStartStamp));
    }

    /**
     * Perform the ready() and sync() actions in a separate thread
     */
    private void ready() {
        String asyncEnabled = System.getProperty("com.sun.jbi.AsyncReadySync", "true"); // Asynch is enabled by default
        if (Boolean.parseBoolean(asyncEnabled)) {
            mSyncThread = new Thread(this, "JBI-Ready-Sync");
            mSyncThread.setDaemon(true);
            mSyncThread.start();
        } else {
            run();
        }
    }

    /**
     * Stop any ready/synchronization action that may be in process.
     */
    private void stopSync() {
        if (mSyncThread != null) {
            if (mSyncProcessor != null) {
                mSyncProcessor.stop();
            }
            try {
                mSyncThread.join(60000);
            } catch (java.lang.InterruptedException iEx) {

            } finally {
                mSyncThread = null;
            }
        }
    }

    /**
     * Have Framework bring the environment to ready, and then perform
     * synchronization actions.
     */
    @Override
    public void run() {

        try {
            super.ready(true);
            {
                String syncEnabled = System.getProperty("com.sun.jbi.SyncEnabled", "true"); // synch is enabled by
                // default
                if (Boolean.parseBoolean(syncEnabled) && mSyncProcessor != null) {
                    mSyncProcessor.start();
                } else {
                    mLogger.warning(mTranslator.getString(LocalStringKeys.JBI_SYNC_DISABLED));
                }
                mSyncProcessor = null;
            }
        } catch (Exception ex) {

        } finally {
            mSyncThread = null;
        }
    }

    /**
     * Signal that particular SunJBIFramework phase has been completed.
     */
    synchronized void endPhase(int phase) {
        mLogger.log(Level.FINE, "JBI:endPhase-{0}: {1}", new Object[]{phase, System.currentTimeMillis() - mStartStamp});
        mPhase = phase;
        this.notifyAll();
    }

    /**
     * Wait for a particular SunJBIFramework phase to be completed.
     */
    synchronized void waitForPhase(int phase) {
        while (phase > mPhase) {
            try {
                this.wait();
            } catch (InterruptedException iEx) {
            }
        }
        mLogger.log(Level.FINE, "JBI:waitForPhase-{0}: {1}", new Object[]{phase, System.currentTimeMillis() - mStartStamp});
    }

    /**
     * This method returns the startup time for the JBI Framework. It may be
     * overridden for specific AS types. Sun AS provider includes the cost of
     * synchronization.
     *
     * @return long - time for startup
     */
    @Override
    public long getStartupTime() {
        return (mStartTime + super.getStartupTime());
    }

}
