package net.openesb.runtime.gfv4;

/**
 *
 */
public class SyncException extends Exception {

    /**
     * Creates a new instance of SyncException with an exception message.
     *
     * @param aMessage String describing this exception.
     */
    public SyncException(String aMessage) {
        super(aMessage);
    }

    /**
     * Creates a new instance of SyncException with the specified message and
     * cause.
     *
     * @param aMessage String describing this exception.
     * @param aCause Throwable which represents an underlying problem (or null).
     */
    public SyncException(String aMessage, Throwable aCause) {
        super(aMessage, aCause);
    }

    /**
     * Creates a new instance of SyncException with the specified cause.
     *
     * @param aCause Throwable which represents an underlying problem (or null).
     */
    public SyncException(Throwable aCause) {
        super(aCause);
    }
}
