package net.openesb.runtime.gfv4;

import java.lang.management.ManagementFactory;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.transaction.TransactionManager;

import org.glassfish.internal.api.ServerContext;

import com.sun.enterprise.config.serverbeans.Cluster;
import com.sun.enterprise.config.serverbeans.Config;
import com.sun.enterprise.config.serverbeans.Domain;
import com.sun.enterprise.config.serverbeans.ModuleLogLevels;
import com.sun.enterprise.config.serverbeans.Server;
import com.sun.enterprise.config.serverbeans.ServerRef;
import com.sun.jbi.JBIProvider;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.platform.PlatformEventListener;
import com.sun.jbi.security.KeyStoreUtil;
import java.util.Collections;
import java.util.logging.Level;
import net.openesb.security.SecurityProvider;

/**
 * This is the <CODE>PlatformContext</CODE> implementation for the GlassFish
 * Server.
 *
 * This class is loaded by the <CODE>GlassFishJBIFramework</CODE> class at
 * startup and is available to the entire JBI runtime framework through the
 * global <CODE>EnvironmentContext</CODE> singleton.
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class GlassFishPlatformContext implements PlatformContext, NotificationListener {

    private Logger mLog;
    private ServerContext context;
    private Domain domain;
    private Config config;
    private MBeanServer mMBeanServer;

    private List<PlatformEventListener> mListeners;

    private KeyStoreUtil mKeyStoreUtil;
    private boolean mMultipleServers;
    private boolean mQueriedMS;

    private static final String SERVER = "server";
    private static final String DOMAIN = "domain";
    private static final String DEFAULT_CONFIG = "default-config";
    private static final String CFG_SFX = "-config";
    private static final String MBS_DELEGATE_NAME = "JMImplementation:type=MBeanServerDelegate";

    private static final String INSTANCE_ROOT_KEY = "com.sun.aas.instanceRoot";
    private static final String INSTALL_ROOT_KEY = "com.sun.aas.installRoot";

    /**
     * Create a new GlassFishPlatformContext.
     */
    public GlassFishPlatformContext(ServerContext context, Domain domain, Config config) {
        this.context = context;
        this.domain = domain;
        this.config = config;
        mMBeanServer = ManagementFactory.getPlatformMBeanServer();
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
    }

	//
    // PlatformContext methods
    //
    /**
     * Get the names of all clustered servers in the domain.
     *
     * @return a set of names of clustered servers in the domain.
     */
    public Set<String> getClusteredServerNames() {
        return Collections.<String>emptySet();
        /*
        HashSet<String> clusteredServers = new HashSet<String>();
        for (Cluster cluster : domain.getClusters().getCluster()) {
            clusteredServers.addAll(getServersInCluster(cluster.getName()));
        }
        mLog.log(Level.FINEST, "Clustered servers in the domain are {0}", clusteredServers.toString());
        return clusteredServers;
        */
    }

    /**
     * Get the names of all clusters in the domain.
     *
     * @return a set of names of clusters in the domain.
     */
    public Set<String> getClusterNames() {
        return Collections.<String>emptySet();
        /*
        HashSet<String> clusterNames = new HashSet<String>();
        for (Cluster cluster : domain.getClusters().getCluster()) {
            clusterNames.add(cluster.getName());
        }
        return clusterNames;
        */
    }

    /**
     * Get a string representation of the DAS JMX RMI Connector port.
     *
     * @return the DAS JMX RMI connector port as a string.
     */
    public String getJmxRmiPort() {
        return config.getAdminService().getSystemJmxConnector().getPort();
    }

    /**
     * Get the name of this instance.
     *
     * @return the name of this server instance.
     */
    public String getInstanceName() {
        return context.getInstanceName();
    }

    /**
     * Get the MBeanServer connection for a server instance.
     *
     * @param instanceName the name of the server instance.
     * @return the MBeanServerConnection for the instance.
     */
    public MBeanServerConnection getMBeanServerConnection(String instanceName) throws Exception {
		// return mASContext.getMBeanServerConnection(instanceName);
        // If the instance name is the same as the local instance return the
        // local MBeanServer
        // TODO: FIX ME
        if (getInstanceName().equals(instanceName)) {
            return getMBeanServer();
        }
        return null;
    }

    /**
     * @return a set of names of all stand alone servers in the domain.
     */
    public Set<String> getStandaloneServerNames() {
        HashSet<String> servSet = new HashSet<String>();
        for (Server server : domain.getServers().getServer()) {
            if (isStandaloneServer(server.getName())) {
                servSet.add(server.getName());
            }
        }
        return servSet;
    }

    /**
     * Get the JBI system class loader for this implementation. This is the JBI
     * common classloader and is the parent of the JBI runtime classloader that
     * loaded this class. The parent of the JBI common class loader is the
     * appserver's shared class loader.
     *
     * See http://docs.sun.com/source/819-0079/dgdeploy.html#wp58491
     *
     * @return the <CODE>ClassLoader</CODE> that is the "system" class loader
     * from a JBI runtime perspective.
     * @throws SecurityException if access to the class loader is denied.
     */
    public ClassLoader getSystemClassLoader() throws SecurityException {
        return context.getLifecycleParentClassLoader();
    }

    /**
     * Get the target name. If the instance is not part of a cluster, then the
     * target is the instance name. If it is a part of a cluster then the target
     * is the cluster name.
     *
     * @return the target name.
     */
    public String getTargetName() {
        return getTargetName(getInstanceName());
    }

    /**
     * Get the target name for a specified instance. If the instance is not part
     * of a cluster, the target name is the instance name. If the instance is
     * part of a cluster, the target name is the cluster name. This operation is
     * invoked by the JBI instance MBeans only.
     *
     * @param instanceName the name of the instance.
     * @return the target name.
     */
    public String getTargetName(String instanceName) {
        return instanceName;
        /*
        String targetName = instanceName;
        if (isInstanceClustered(instanceName)) {
            // -- Target is the cluster name
            Cluster cluster = domain.getClusterForInstance(instanceName);
            if (cluster != null) {
                targetName = cluster.getName();
            }
        }
        return targetName;
        */
    }

    /**
     * Get the names of all of the servers in a cluster.
     *
     * @param clusterName the name of the cluster.
     * @return a Set of the names of the servers in the cluster.
     */
    public Set<String> getServersInCluster(String clusterName) {
        return Collections.<String>emptySet();
        /*
        HashSet<String> clusteredServers = new HashSet<String>();
        for (ServerRef server : domain.getClusterNamed(clusterName).getServerRef()) {
            clusteredServers.add(server.getRef());
        }
        return clusteredServers;
        */
    }

    /**
     * Get the implementation of the TransactionManager.
     *
     * @return the TransactionManager implementation.
     * @throws Exception if the TransactionManager instance cannot be obtained.
     */
    public TransactionManager getTransactionManager() throws Exception {
        TransactionManager transactionManager = (javax.transaction.TransactionManager) context.getInitialContext().lookup("java:appserver/TransactionManager");
        return transactionManager;
    }

    /**
     * Determine whether or not a target is a cluster.
     *
     * @param targetName the name of the target.
     * @return true if the target is a cluster, false if not.
     */
    public boolean isCluster(String targetName) {
        return false;
//        return domain.getClusterNamed(targetName) != null;
    }

    /**
     * Determine whether or not a target is a clustered server.
     *
     * @param targetName the name of the target.
     * @return true if the target is a clustered server, false if not.
     */
    public boolean isClusteredServer(String targetName) {
        return false;
//        return getClusteredServerNames().contains(targetName);
    }

    /**
     * Get the instance name of the platform's administration server. If the
     * platform does not provide a separate administration server, then this
     * method returns the name of the local instance.
     *
     * @return instance name of the administration server
     */
    public String getAdminServerName() {
        return getInstanceName();
    }

    /**
     * Determine whether or not this instance is the administration server.
     *
     * @return true if this instance is the DAS, false if not.
     */
    public boolean isAdminServer() {
        return true;
        // Each instance is providing its own Rest API
//        return context.getConfigBean().isDas();
    }

    /**
     * Determine whether or not an instance is a member of a cluster. NOTE: if
     * an exception occurs while querying the platform implementation for this
     * information, the error is logged and false is returned.
     *
     * @param instanceName the name of the instance.
     * @return true if the instance is a member of a cluster, false if not.
     */
    public boolean isInstanceClustered(String instanceName) {
        return false;
//        return domain.getClusterForInstance(instanceName) != null;
    }

    /**
     * Determine whether or not an instance is up and running.
     *
     * @param instanceName the name of the instance.
     * @return true if the instance is up and running, false if not.
     */
    public boolean isInstanceUp(String instanceName) {
        return domain.getServerNamed(instanceName).isRunning();
    }

    /**
     * Determine whether or not a target is a standalone server.
     *
     * @param targetName the name of the target.
     * @return true if the target is a standalone server, false if not.
     */
    public boolean isStandaloneServer(String targetName) {
        return true;
//        Server server = domain.getServerNamed(targetName);
//        return server != null && domain.getClusterForInstance(targetName) == null;
    }

    /**
     * Determine whether or not a target is a valid standalone server name or
     * cluster name.
     *
     * @param targetName the name of the target.
     * @return true if the target is a valid standalone server or cluster, false
     * if not.
     */
    public boolean isValidTarget(String targetName) {
        boolean isValid = false;
        if (isCluster(targetName) || isStandaloneServer(targetName)) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Determine whether or not multiple servers are permitted for this domain.
     * *
     *
     * @return true if the installation supports multiple servers, false if not.
     */
    public synchronized boolean supportsMultipleServers() {
        if (!mQueriedMS) {
			// mMultipleServers = mASContext.multipleServersSupported();
            // TODO: FIX ME
            if (isAdminServer()) {
                mMultipleServers = true;
            } else {
                mMultipleServers = false;
            }
            mQueriedMS = true;
        }

        return (mMultipleServers);
    }

    /**
     * Provides access to the platform's MBean server.
     *
     * @return platform MBean server.
     */
    public MBeanServer getMBeanServer() {
        return mMBeanServer;
    }

    /**
     * Get the full path to the platform's instance root directory.
     *
     * @return platform instance root
     */
    public String getInstanceRoot() {
        return System.getProperty(INSTANCE_ROOT_KEY);
    }

    /**
     * Get the full path to the platform's instaall root directory.
     *
     * @return platform install root
     */
    public String getInstallRoot() {
        return System.getProperty(INSTALL_ROOT_KEY);
    }

    /**
     * Returns the provider type for this platform.
     *
     * @return enum value corresponding to this platform implementation.
     */
    public JBIProvider getProvider() {
        return JBIProvider.GLASSFISH_V4;
    }

    /**
     * Returns the KeyStoreUtil for the Sun Platform
     *
     * @return the KeyStoreUtil for the Sun Platform
     */
    synchronized public KeyStoreUtil getKeyStoreUtil() {
        if (mKeyStoreUtil == null) {
            mKeyStoreUtil = new GlassFishKeyStoreUtil();
        }
        return mKeyStoreUtil;
    }

	//
    // NotificationListener methods
    //
    /**
     *
     * @param event - the notification event
     * @param handback - callback object, not used by this implementation
     */
    public void handleNotification(javax.management.Notification event, Object handback) {

        if (event instanceof javax.management.MBeanServerNotification) {
            javax.management.MBeanServerNotification mbnEvent = (javax.management.MBeanServerNotification) event;

            ObjectName mbnName = mbnEvent.getMBeanName();
            if (isInstanceMBean(mbnName)) {
                String instanceName = mbnName.getKeyProperty("name");
                broadcastInstanceEvent(mbnEvent, instanceName);
            } else if (isClusterMBean(mbnName)) {
                String clusterName = mbnName.getKeyProperty("name");
                broadcastClusterEvent(mbnEvent, clusterName);
            }
        }
    }

	//
    // AdminEventBroadcaster methods
    //
    /**
     * Register an AdminEventListener.
     *
     * @param listener an event listener to be registered.
     */
    public synchronized void addListener(PlatformEventListener listener) {
        List<PlatformEventListener> listeners = getAdminListeners();

		// We only register our platform MBean registration listener when the
        // first event listener is added.
        if (listeners.isEmpty()) {
            registerPlatformListener();
        }

        listeners.add(listener);
    }

    /**
     * Remove an AdminEventListener.
     *
     * @param listener an event listener to be removed.
     */
    public synchronized void removeListener(PlatformEventListener listener) {
        List<PlatformEventListener> listeners = getAdminListeners();
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }

        if (listeners.isEmpty()) {
			// Clean up the platform MBean registration listener, since no
            // event listeners are registered.
            unregisterPlatformListener();
        }
    }

    /**
     * Retrieves the naming context that should be used to locate platform
     * resources (e.g. TransactionManager).
     *
     * @return naming context
     */
    public InitialContext getNamingContext() {
        return context.getInitialContext();
    }

    /**
     * Get the "com.sun.jbi" log level for a target.
     *
     * @param target - target name
     * @return the default platform log level
     */
    public java.util.logging.Level getJbiLogLevel(String target) {
        java.util.logging.Level defLevel = java.util.logging.Level.INFO;

        if (isAdminServer()) {
            // TODO: FIX ME
            String configName = getTargetConfigName(target);
            Config config = domain.getConfigNamed(configName);
            ModuleLogLevels mll = config.getLogService().getModuleLogLevels();
            defLevel = java.util.logging.Level.parse(mll.getPropertyValue(JBI_LOGGER_NAME));
        } else {
            // Get the com.sun.jbi level set in the VM
            defLevel = java.util.logging.Logger.getLogger(JBI_LOGGER_NAME).getLevel();
        }

        return defLevel;
    }

    /**
     * Set the "com.sun.jbi" log level for a target.
     *
     * @param level the default platform log level
     */
    public void setJbiLogLevel(String target, java.util.logging.Level level) {
        if (isAdminServer()) {
			// TODO: FIX ME
            // ModuleLogLevelsConfig mll = getModuleLogLevelsConfig(target);
            // if (mll != null) {
            // if (level != null) {
            // if (!mll.existsProperty(JBI_LOGGER_NAME)) {
            // mll.createProperty(JBI_LOGGER_NAME, level.toString());
            // } else {
            // mll.setPropertyValue(JBI_LOGGER_NAME, level.toString());
            // }
            // } else {
            // // delete the property to inherit from the parent
            // mll.removeProperty(JBI_LOGGER_NAME);
            // }
            //
            // }
        }
    }

	//
    // Private methods
    //
    /**
     * Get a list of all registered AdminEventListeners.
     *
     * @return a List of AdminEventListeners, which can be empty if there are no
     * listeners registered.
     */
    private synchronized List<PlatformEventListener> getAdminListeners() {
        if (mListeners == null) {
            mListeners = new java.util.ArrayList<PlatformEventListener>();
        }
        return mListeners;
    }

    /**
     * @return true if this is a instance Mbean object name
     */
    private boolean isInstanceMBean(ObjectName mbnName) {
        return mbnName.toString().startsWith("amx:j2eeType=J2EEServer,name=");
    }

    /**
     * @return true if this is a Cluster MBean
     */
    private boolean isClusterMBean(ObjectName mbnName) {
        return mbnName.toString().startsWith("amx:j2eeType=X-J2EECluster,name=");
    }

    /**
     * Broadcast the MBean registration / unregistration event for an instance
     * MBean.
     *
     * @param mbnEvent - MBeanServerNotification
     * @param instanceName - instance name
     */
    private void broadcastInstanceEvent(javax.management.MBeanServerNotification mbnEvent, String instanceName) {
        List<PlatformEventListener> listeners = getAdminListeners();
        if (mbnEvent.getType().equalsIgnoreCase("JMX.mbean.registered")) {
            for (PlatformEventListener listener : listeners) {
                try {
                    listener.createdInstance(instanceName);
                } catch (Exception ex) {
                    mLog.warning(ex.toString());
                    continue;
                }
            }
        } else if (mbnEvent.getType().equalsIgnoreCase("JMX.mbean.unregistered")) {
            for (PlatformEventListener listener : listeners) {
                /**
                 * Here we even notify listeners of cluster instance deletion
                 * notifications, the listener should perform cleanup tasks only
                 * for registered standalone instances. isStandaloneServer()
                 * returns false after the MBean is unregistered even if the
                 * server is standalone, hence the decision to pass through
                 * cluster instance notifications
                 */
                try {
                    listener.deletedInstance(instanceName);
                } catch (Exception ex) {
                    mLog.warning(ex.toString());
                    continue;
                }
            }

        }
    }

    /**
     * Broadcast the MBean registration / unregistration event for a cluster
     * MBean.
     *
     * @param mbnEvent - MBeanServerNotification
     * @param clusterName - cluster name
     */
    private void broadcastClusterEvent(javax.management.MBeanServerNotification mbnEvent, String clusterName) {
        List<PlatformEventListener> listeners = getAdminListeners();
        if (mbnEvent.getType().equalsIgnoreCase("JMX.mbean.registered")) {
            for (PlatformEventListener listener : listeners) {
                try {
                    listener.createdCluster(clusterName);
                } catch (Exception ex) {
                    mLog.warning(ex.toString());
                    continue;
                }
            }
        } else if (mbnEvent.getType().equalsIgnoreCase("JMX.mbean.unregistered")) {
            for (PlatformEventListener listener : listeners) {
                try {
                    listener.deletedCluster(clusterName);
                } catch (Exception ex) {
                    mLog.warning(ex.toString());
                    continue;
                }
            }

        }
    }

    /**
     * Register a notification listener to catch MBean registration activity in
     * the platform MBean server.
     */
    private void registerPlatformListener() {
        if (isAdminServer() && supportsMultipleServers()) {
            try {
                mMBeanServer.addNotificationListener(new ObjectName(MBS_DELEGATE_NAME), this, null, null);
            } catch (Exception ex) {
                mLog.warning(ex.toString());
            }
        }
    }

    /**
     * Remove the notification listener to catch MBean registration activity in
     * the platform MBean server.
     */
    private void unregisterPlatformListener() {
        try {
            mMBeanServer.removeNotificationListener(new ObjectName(MBS_DELEGATE_NAME), this);
        } catch (Exception ex) {
            mLog.warning(ex.toString());
        }
    }

    /**
     * Get the config name for the target.
     *
     * @param target - target name
     * @return the config name for a target, this is the target name appended
     * with "-config"
     */
    private String getTargetConfigName(String target) {
        String configName = DEFAULT_CONFIG;

        if (target != null) {
            if (!target.equals(DOMAIN)) {
                String targetName = (isClusteredServer(target) ? getTargetName(target) : target);

                configName = targetName + CFG_SFX;
            }
        }
        return configName;
    }

    @Override
    public SecurityProvider getSecurityProvider() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the ObjectName pattern String for the ModuleLogLevelsConfig AMX
     * MBean
     */
	// private String getModuleLogLevelsConfigNamePattern(String target) {
    // String targetName = isClusteredServer(target) ? getTargetName() : target;
    //
    // /**
    // * return "amx:X-ConfigConfig=" + getTargetConfigName(targetName) +
    // * ",j2eeType=X-ModuleLogLevelsConfig,name=na,X-LogServiceConfig=na";
    // */
    // return "com.sun.appserv:type=module-log-levels,config=" + getTargetConfigName(targetName) + ",category=config";
    // }
    /**
     * @return the Glassfish ModuleLogLevelsConfig instance
     */
	// private ModuleLogLevelsConfig getModuleLogLevelsConfig(String target) {
    // ModuleLogLevelsConfig mll = null;
    //
    // String configName = getTargetConfigName(target);
    //
    // DomainRoot domain = ProxyFactory.getInstance(mMBeanServer).getDomainRoot();
    //
    // if (domain != null) {
    // DomainConfig domainConfig = domain.getDomainConfig();
    //
    // if (domainConfig != null) {
    // Map<java.lang.String, com.sun.appserv.management.config.ConfigConfig> cfgMap = domainConfig.getConfigConfigMap();
    // ConfigConfig config = cfgMap.get(configName);
    //
    // if (config != null) {
    // mll = config.getLogServiceConfig().getModuleLogLevelsConfig();
    // }
    // }
    // }
    // return mll;
    // }
}
