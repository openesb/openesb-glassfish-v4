package net.openesb.runtime.gfv4;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.glassfish.internal.api.Globals;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.sun.enterprise.security.store.IdentityManagement;
import com.sun.enterprise.security.store.PasswordAdapter;
import com.sun.jbi.security.KeyStoreUtil;

/**
 *
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class GlassFishKeyStoreUtil implements KeyStoreUtil {

    private static final String DEFAULT_ALGORITHM = "AES";
    private static final String DEFAULT_KEYNAME = "GlassFishKeyStoreUtil";

    private BASE64Encoder mBase64Encoder;
    private BASE64Decoder mBase64Decoder;

    public GlassFishKeyStoreUtil() {
        mBase64Encoder = new BASE64Encoder();
        mBase64Decoder = new BASE64Decoder();
    }

    /**
     * Generates a random key for the alias and stores the key in the key store
     * for the alias.
     *
     * @param keyName the name of the key
     * @exception KeyStoreException if the key already exists in the KeyStore
     */
    public void addKey(String keyName) throws KeyStoreException {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance(DEFAULT_ALGORITHM);
            SecretKey secretKey = keygen.generateKey();
            PasswordAdapter p = getPasswordAdapter();
            if (p.aliasExists(keyName)) {
                throw new KeyStoreException("Password Alias Exists");
            }
            p.setPasswordForAlias(keyName, secretKey.getEncoded());
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Checks to see if a key referenced by keyName exists in the KeyStore
     *
     * @param keyName the name of the Key
     * @return true if the key exists in the KeyStore, false otherwise
     * @exception KeyStoreException if any error occurs.
     */
    public boolean hasKey(String keyName) throws KeyStoreException {
        try {
            PasswordAdapter p = getPasswordAdapter();
            return p.aliasExists(keyName);
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Updates an existing alias in the KeyStore when a new randomly generated
     * key.
     *
     * @param keyName the name of the key
     * @exception KeyStoreException if the keyName does not already exist in the
     * KeyStore
     */
    public void updateKey(String keyName) throws KeyStoreException {
        try {
            PasswordAdapter p = getPasswordAdapter();
            if (!p.aliasExists(keyName)) {
                throw new KeyStoreException("Password Alias Does Not Exist");
            }
            KeyGenerator keygen = KeyGenerator.getInstance(DEFAULT_ALGORITHM);
            SecretKey secretKey = keygen.generateKey();
            p.setPasswordForAlias(keyName, secretKey.getEncoded());
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Deletes a key from the KeyStore.
     *
     * @param keyName the name of the key
     * @exception KeyStoreException if the key can't be deleted
     */
    public void deleteKey(String keyName) throws KeyStoreException {
        try {
            PasswordAdapter p = getPasswordAdapter();
            if (!p.aliasExists(keyName)) {
                throw new KeyStoreException("Password Alias Does Not Exist");
            }
            p.removeAlias(keyName);
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Lists all key names in the KeyStore
     *
     * @return the list of key names as a String array
     * @exception KeyStoreException if any error occurs while trying to retrieve
     * the list of key names
     */
    public String[] listKeyNames() throws KeyStoreException {
        try {
            ArrayList result = new ArrayList();
            Enumeration en = getPasswordAdapter().getAliases();
            while (en.hasMoreElements()) {
                result.add((String) en.nextElement());
            }
            return (String[]) result.toArray(new String[result.size()]);
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Retrieves the key as a string
     *
     * @param keyName the name of the key
     * @return the key as a plain-text.
     * @exception KeyStoreException if any error occurs while trying to retrieve
     * the key
     */
    public String getKey(String keyName) throws KeyStoreException {
        try {
            PasswordAdapter p = getPasswordAdapter();
            return p.getPasswordForAlias(keyName);
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Encrypts a message using the key identified by keyName
     *
     * @param keyName the name of the key
     * @param cleartext the byte array that will be encrypted
     * @return the encrypted byte array
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    public byte[] encrypt(String keyName, byte[] cleartext) throws KeyStoreException {

        try {
            PasswordAdapter p = getPasswordAdapter();
            SecretKey instKey = p.getPasswordSecretKeyForAlias(keyName);
			// String keyValue = p.getPasswordForAlias(keyName);
            // SecretKey instKey = new SecretKeySpec(keyValue.getBytes(), DEFAULT_ALGORITHM);

            // Create the cipher
            Cipher cipher = Cipher.getInstance(DEFAULT_ALGORITHM);

            // Initialize the cipher for encryption
            cipher.init(Cipher.ENCRYPT_MODE, instKey);

            // Encrypt the cleartext
            byte[] ciphertext = cipher.doFinal(cleartext);

            return ciphertext;
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Decrypts a message using the key identified by keyName
     *
     * @param keyName the name of the key
     * @param cipherText the byte array with the encrypted data
     * @return the unencrypted byte array
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    public byte[] decrypt(String keyName, byte[] cipherText) throws KeyStoreException {
        try {
            PasswordAdapter p = getPasswordAdapter();
            SecretKey instKey = p.getPasswordSecretKeyForAlias(keyName);
			// String keyValue = p.getPasswordForAlias(keyName);
            // SecretKey instKey = new SecretKeySpec(keyValue.getBytes(), DEFAULT_ALGORITHM);

            // Create the cipher
            Cipher cipher = Cipher.getInstance(DEFAULT_ALGORITHM);

            // Initialize the cipher for encryption
            cipher.init(Cipher.DECRYPT_MODE, instKey);

            // Encrypt the cleartext
            byte[] cleartext = cipher.doFinal(cipherText);

            return cleartext;
        } catch (KeyStoreException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new KeyStoreException(ex);

        }
    }

    /**
     * Encrypts a message using the key identified by keyName. The result is a
     * Base64-encoded string.
     *
     * @param keyName the name of the key
     * @param clearText a String representing the message to be encrypted
     * @return a Base64-encoded string representing the encrypted message
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    public String encrypt(String keyName, String clearText) throws KeyStoreException {
        byte[] cipherText = encrypt(keyName, clearText.getBytes());
        return mBase64Encoder.encode(cipherText);
    }

    /**
     * Decrypts a message using the key identified by keyName. The second
     * argument must be a Base-64 encoded string
     *
     * @param keyName the name of the key
     * @param base64EncodedCipherText a Base-64 Encoded string
     * @return the decrypted message as a String
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    public String decrypt(String keyName, String base64EncodedCipherText) throws KeyStoreException {
        try {
            byte[] clearText = decrypt(keyName, mBase64Decoder.decodeBuffer(base64EncodedCipherText));
            return new String(clearText);
        } catch (Exception ex) {
            throw new KeyStoreException(ex);
        }
    }

    /**
     * Encrypts a message using a default key.
     *
     * @param clearText the byte array that will be encrypted
     * @return the encrypted byte array
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    @Override
    public byte[] encrypt(byte[] clearText) throws KeyStoreException {
        initDefaultKey();
        return encrypt(DEFAULT_KEYNAME, clearText);
    }

    /**
     * Decrypts a message using a default key
     *
     * @param cipherText the byte array with the encrypted data
     * @return the unencrypted byte array
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    @Override
    public byte[] decrypt(byte[] cipherText) throws KeyStoreException {
        return decrypt(DEFAULT_KEYNAME, cipherText);
    }

    /**
     * Encrypts a message using a default key. The result is a Base64-encoded
     * string.
     *
     * @param clearText a String representing the message to be encrypted
     * @return a Base64-encoded string representing the encrypted message
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    @Override
    public String encrypt(String clearText) throws KeyStoreException {
        initDefaultKey();
        return encrypt(DEFAULT_KEYNAME, clearText);
    }

    /**
     * Decrypts a message using the key identified by keyName. The second
     * argument must be a Base-64 encoded string
     *
     * @param base64EncodedCipherText a Base-64 Encoded string
     * @return the decrypted message as a String
     * @exception KeyStoreException if any error occurs retrieving the key to be
     * used
     */
    @Override
    public String decrypt(String base64EncodedCipherText) throws KeyStoreException {
        return decrypt(DEFAULT_KEYNAME, base64EncodedCipherText);
    }

    private void initDefaultKey() throws KeyStoreException {
        if (!hasKey(DEFAULT_KEYNAME)) {
            addKey(DEFAULT_KEYNAME);
        }
    }

    private PasswordAdapter getPasswordAdapter() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException {
        return new PasswordAdapter(Globals.getDefaultHabitat().getService(IdentityManagement.class).getMasterPassword());
    }
}
